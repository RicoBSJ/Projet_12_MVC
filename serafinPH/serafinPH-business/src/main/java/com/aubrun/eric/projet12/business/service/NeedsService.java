package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.consumer.NeedsConsumer;
import com.aubrun.eric.projet12.model.JwtToken;
import com.aubrun.eric.projet12.model.Need;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class NeedsService {

    private final NeedsConsumer needsConsumer;
    private final JwtToken jwtToken;

    public NeedsService(NeedsConsumer needsConsumer, JwtToken jwtToken) {
        this.needsConsumer = needsConsumer;
        this.jwtToken = jwtToken;
    }

    public ResponseEntity<Need> getAllNeeds() {
        return needsConsumer.findAllNeeds(jwtToken);
    }
}
