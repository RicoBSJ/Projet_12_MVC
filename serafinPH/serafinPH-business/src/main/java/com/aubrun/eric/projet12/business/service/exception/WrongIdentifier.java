package com.aubrun.eric.projet12.business.service.exception;

public class WrongIdentifier extends RuntimeException {

    public WrongIdentifier() {
        super("Please verify your login details");
    }


}
