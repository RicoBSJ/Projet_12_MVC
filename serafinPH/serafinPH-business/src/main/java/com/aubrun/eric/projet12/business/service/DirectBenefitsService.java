package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.consumer.DirectBenefitsConsumer;
import com.aubrun.eric.projet12.model.DirectsBenefits;
import com.aubrun.eric.projet12.model.JwtToken;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DirectBenefitsService {

    private final DirectBenefitsConsumer directBenefitsConsumer;
    private final JwtToken jwtToken;

    public DirectBenefitsService(DirectBenefitsConsumer directBenefitsConsumer, JwtToken jwtToken) {
        this.directBenefitsConsumer = directBenefitsConsumer;
        this.jwtToken = jwtToken;
    }

    public ResponseEntity<DirectsBenefits> getAllDirectBenefits() {
        return directBenefitsConsumer.findAllDirectBenefits(jwtToken);
    }
}
