package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.consumer.JuridicProtectionConsumer;
import com.aubrun.eric.projet12.model.JuridicProtection;
import com.aubrun.eric.projet12.model.JuridicProtections;
import com.aubrun.eric.projet12.model.JwtToken;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class JuridicProtectionService {

    private final JuridicProtectionConsumer juridicProtectionConsumer;
    private final JwtToken jwtToken;

    public JuridicProtectionService(JuridicProtectionConsumer juridicProtectionConsumer, JwtToken jwtToken) {
        this.juridicProtectionConsumer = juridicProtectionConsumer;
        this.jwtToken = jwtToken;
    }

    public ResponseEntity<JuridicProtections> getAllJuridicProtection() {
        return juridicProtectionConsumer.findAllJuridicProtections(jwtToken);
    }

    public ResponseEntity<JuridicProtection> findJuridicProtectionById(int juridicProtectionId){
        return juridicProtectionConsumer.juridicProtectionById(juridicProtectionId);
    }
}
