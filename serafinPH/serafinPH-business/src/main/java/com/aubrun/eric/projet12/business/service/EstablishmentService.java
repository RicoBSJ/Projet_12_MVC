package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.consumer.EstablishmentConsumer;
import com.aubrun.eric.projet12.model.Establishments;
import com.aubrun.eric.projet12.model.JwtToken;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EstablishmentService {

    private final EstablishmentConsumer establishmentConsumer;
    private final JwtToken jwtToken;

    public EstablishmentService(EstablishmentConsumer establishmentConsumer, JwtToken jwtToken) {
        this.establishmentConsumer = establishmentConsumer;
        this.jwtToken = jwtToken;
    }

    public ResponseEntity<Establishments> getAllEstablishments() {
        return establishmentConsumer.findAllEstablishments(jwtToken);
    }
}
