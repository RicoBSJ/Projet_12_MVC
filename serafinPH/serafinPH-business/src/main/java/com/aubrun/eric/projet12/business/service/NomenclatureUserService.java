package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.consumer.EstablishmentConsumer;
import com.aubrun.eric.projet12.consumer.NomenclatureUserConsumer;
import com.aubrun.eric.projet12.consumer.RoleConsumer;
import com.aubrun.eric.projet12.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Service
@Transactional
public class NomenclatureUserService {

    private final NomenclatureUserConsumer nomenclatureUserConsumer;
    private final EstablishmentConsumer establishmentConsumer;
    private final RoleConsumer roleConsumer;
    private final JwtToken jwtToken;

    public NomenclatureUserService(NomenclatureUserConsumer nomenclatureUserConsumer, EstablishmentConsumer establishmentConsumer, RoleConsumer roleConsumer, JwtToken jwtToken) {
        this.nomenclatureUserConsumer = nomenclatureUserConsumer;
        this.establishmentConsumer = establishmentConsumer;
        this.roleConsumer = roleConsumer;
        this.jwtToken = jwtToken;
    }

    public ResponseEntity<NomenclatureUser> addUser(AddNomenclatureUser addNomenclatureUser) {

        return nomenclatureUserConsumer.addUserAccount(addNomenclatureUser, jwtToken);
    }

    public ResponseEntity<NomenclatureUsers> getAllUsers() {

        return nomenclatureUserConsumer.getUsers(jwtToken);
    }

    public ResponseEntity<NomenclatureUser> getUserById(int userId) {
        return nomenclatureUserConsumer.userById(userId, jwtToken);
    }

    public ResponseEntity<JwtResponse> login(String email, String password) {
        NomenclatureUser nomenclatureUser = new NomenclatureUser();
        nomenclatureUser.setEmail(email);
        nomenclatureUser.setPassword(password);
        ResponseEntity<JwtResponse> response = nomenclatureUserConsumer.login(nomenclatureUser);
        if (response.getStatusCode() == HttpStatus.OK && response.getBody() != null ) {
            this.jwtToken.setJwt(response.getBody().getAccessToken());
        }
        return response;
    }

    public ResponseEntity<Void> updateNomenclatureUser(int nomenclatureUserId, String releaseDateUser) {
        LocalDateTime dateRelease = LocalDateTime.parse(releaseDateUser, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        NomenclatureUser nomenclatureUser = nomenclatureUserConsumer.userById(nomenclatureUserId, jwtToken).getBody();
        Objects.requireNonNull(nomenclatureUser).setReleaseDateUser(dateRelease);
        return nomenclatureUserConsumer.updateNomenclatureUser(nomenclatureUser, jwtToken);
    }

    public ResponseEntity<Void> addEstablishment(int nomenclatureUserId, int establishmentId){
        Establishment establishment = establishmentConsumer.establishmentById(establishmentId).getBody();
        Establishment establishments = new Establishment();
        establishments.setEstablishmentId(Objects.requireNonNull(establishment).getEstablishmentId());
        establishments.setEstablishmentName(establishment.getEstablishmentName());
        Set<Establishment> establishmentSet = new HashSet<>();
        establishmentSet.add(establishments);
        NomenclatureUser nomenclatureUser = new NomenclatureUser();
        nomenclatureUser.setNomenclatureUserId(nomenclatureUserId);
        nomenclatureUser.setEstablishments(establishmentSet);
        return nomenclatureUserConsumer.addEstablishment(nomenclatureUser.getNomenclatureUserId(), establishments.getEstablishmentId(), jwtToken);
    }

    public ResponseEntity<Void> addRole(int nomenclatureUserId, int roleId){
        Role role = roleConsumer.roleById(roleId).getBody();
        Role roles = new Role();
        roles.setRoleId(Objects.requireNonNull(role).getRoleId());
        roles.setRoleName(role.getRoleName());
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(roles);
        NomenclatureUser nomenclatureUser = new NomenclatureUser();
        nomenclatureUser.setNomenclatureUserId(nomenclatureUserId);
        nomenclatureUser.setRoles(roleSet);
        return nomenclatureUserConsumer.addRole(nomenclatureUser.getNomenclatureUserId(), roles.getRoleId(), jwtToken);
    }

    public ResponseEntity<Void> deleteEstablishment(int nomenclatureUserEstablishmentId){
        nomenclatureUserConsumer.getUsers(jwtToken);
        return nomenclatureUserConsumer.deleteEstablishment(nomenclatureUserEstablishmentId, jwtToken);
    }

    public ResponseEntity<Void> deleteRole(int nomenclatureUserRoleId){
        nomenclatureUserConsumer.getUsers(jwtToken);
        return nomenclatureUserConsumer.deleteRole(nomenclatureUserRoleId, jwtToken);
    }

    public void logout() {
        jwtToken.setJwt(null);
    }
}
