package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.consumer.*;
import com.aubrun.eric.projet12.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Transactional
public class CustomerService {

    private final CustomerConsumer customerConsumer;
    private final NeedsConsumer needsConsumer;
    private final JuridicProtectionConsumer juridicProtectionConsumer;
    private final EstablishmentConsumer establishmentConsumer;
    private final DirectBenefitsConsumer directBenefitsConsumer;
    private final IndirectBenefitsConsumer indirectBenefitsConsumer;
    private final NomenclatureUserConsumer nomenclatureUserConsumer;
    private final JwtToken jwtToken;

    public CustomerService(CustomerConsumer customerConsumer, NeedsConsumer needsConsumer, JuridicProtectionConsumer juridicProtectionConsumer, EstablishmentConsumer establishmentConsumer, DirectBenefitsConsumer directBenefitsConsumer, IndirectBenefitsConsumer indirectBenefitsConsumer, NomenclatureUserConsumer nomenclatureUserConsumer, JwtToken jwtToken) {
        this.customerConsumer = customerConsumer;
        this.needsConsumer = needsConsumer;
        this.juridicProtectionConsumer = juridicProtectionConsumer;
        this.establishmentConsumer = establishmentConsumer;
        this.directBenefitsConsumer = directBenefitsConsumer;
        this.indirectBenefitsConsumer = indirectBenefitsConsumer;
        this.nomenclatureUserConsumer = nomenclatureUserConsumer;
        this.jwtToken = jwtToken;
    }

    public ResponseEntity<Customers> findCustomers(Principal principal) {
        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) principal;
        List<String> authorities = token.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList();
        if (authorities.contains("ROLE_MODERATOR")){
            return customerConsumer.findAllCustomersModerator(jwtToken);
        }
        return customerConsumer.findAllCustomers(jwtToken);
    }

    public ResponseEntity<Customer> findCustomerById(int customerId) {
        return customerConsumer.customerById(customerId, jwtToken);
    }

    public ResponseEntity<Void> addCustomer(AddCustomer addCustomer, int nomenclatureUserId){
        NomenclatureUser nomenclatureUser = nomenclatureUserConsumer.userById(nomenclatureUserId, jwtToken).getBody();
        LocalDate dateBirth = LocalDate.parse(addCustomer.getDateBirth(), DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDateTime dateEntry = LocalDateTime.parse(addCustomer.getEntryDate(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        Customer customer = new Customer();
        customer.setCustomerFirstName(addCustomer.getCustomerFirstName());
        customer.setCustomerLastName(addCustomer.getCustomerLastName());
        customer.setDateBirth(dateBirth);
        customer.setSocialSecurityNumber(addCustomer.getSocialSecurityNumber());
        customer.setMutualName(addCustomer.getMutualName());
        customer.setEntryDate(dateEntry);
        customer.setAge(addCustomer.getAge());
        customer.setNomenclatureUser(String.valueOf(Objects.requireNonNull(nomenclatureUser).getNomenclatureUserId()));
        return customerConsumer.createCustomer(customer, jwtToken);
    }

    public ResponseEntity<Customers> searchCustomer(SearchCustomer searchCustomer){
        return customerConsumer.searchCustomer(searchCustomer, jwtToken);
    }

    public ResponseEntity<Void> addNomenclatureUser(int nomenclatureUserId, int customerId) {
        NomenclatureUser nomenclatureUser = nomenclatureUserConsumer.userById(nomenclatureUserId, jwtToken).getBody();
        Customer customer = customerConsumer.customerById(customerId, jwtToken).getBody();
        Objects.requireNonNull(customer).setNomenclatureUser(Objects.requireNonNull(nomenclatureUser).getLastName());
        Objects.requireNonNull(nomenclatureUser).getCustomerList().add(customer);
        return customerConsumer.addNomenclatureUser(nomenclatureUser, customer.getCustomerId(), jwtToken);
    }

    public ResponseEntity<Void> addDirectBenefits(int customerId, int directBenefitsId) {
        DirectBenefits directBenefitsById = directBenefitsConsumer.directBenefitsById(directBenefitsId).getBody();
        DirectBenefits directBenefits = new DirectBenefits();
        directBenefits.setDirectBenefitsId(Objects.requireNonNull(directBenefitsById).getDirectBenefitsId());
        directBenefits.setDirectBenefitName(directBenefitsById.getDirectBenefitName());
        directBenefits.setDirectBenefitType(directBenefitsById.getDirectBenefitType());
        List<DirectBenefits> directBenefitsList = new ArrayList<>();
        directBenefitsList.add(directBenefits);
        Customer customer = new Customer();
        customer.setCustomerId(customerId);
        customer.setDirectBenefitsList(directBenefitsList);
        return customerConsumer.addDirectBenefits(customer.getCustomerId(), directBenefits.getDirectBenefitsId(), jwtToken);
    }

    public ResponseEntity<Void> addIndirectBenefits(int customerId, int indirectBenefitsId) {
        IndirectBenefits indirectBenefitsById = indirectBenefitsConsumer.indirectBenefitsById(indirectBenefitsId).getBody();
        IndirectBenefits indirectBenefits = new IndirectBenefits();
        indirectBenefits.setIndirectBenefitsId(Objects.requireNonNull(indirectBenefitsById).getIndirectBenefitsId());
        indirectBenefits.setIndirectBenefitName(indirectBenefitsById.getIndirectBenefitName());
        indirectBenefits.setIndirectBenefitType(indirectBenefitsById.getIndirectBenefitType());
        List<IndirectBenefits> indirectBenefitsList = new ArrayList<>();
        indirectBenefitsList.add(indirectBenefits);
        Customer customer = new Customer();
        customer.setCustomerId(customerId);
        customer.setIndirectBenefitsList(indirectBenefitsList);
        return customerConsumer.addIndirectBenefits(customer.getCustomerId(), indirectBenefits.getIndirectBenefitsId(), jwtToken);
    }

    public ResponseEntity<Void> addNeeds(int customerId, int needsId) {
        Needs need = needsConsumer.needsById(needsId).getBody();
        Needs needs = new Needs();
        needs.setNeedsId(Objects.requireNonNull(need).getNeedsId());
        needs.setNeedName(need.getNeedName());
        needs.setNeedType(need.getNeedType());
        List<Needs> needsList = new ArrayList<>();
        needsList.add(needs);
        Customer customer = new Customer();
        customer.setCustomerId(customerId);
        customer.setNeedsList(needsList);
        return customerConsumer.addNeeds(customer.getCustomerId(), needs.getNeedsId() , jwtToken);
    }

    public ResponseEntity<Void> updateJuridicProtection(int customerId, int juridicProtectionId) {
        JuridicProtection protection = juridicProtectionConsumer.juridicProtectionById(juridicProtectionId).getBody();
        JuridicProtection juridicProtection = new JuridicProtection();
        juridicProtection.setJuridicProtectionId(Objects.requireNonNull(protection).getJuridicProtectionId());
        juridicProtection.setJuridicProtectionName(Objects.requireNonNull(protection).getJuridicProtectionName());
        Set<JuridicProtection> juridicProtections = new HashSet<>();
        juridicProtections.add(juridicProtection);
        Customer customer = new Customer();
        customer.setCustomerId(customerId);
        customer.setJuridicProtectionList(juridicProtections);
        return customerConsumer.updateJuridicProtection(customer.getCustomerId(), protection.getJuridicProtectionId(), jwtToken);
    }

    public ResponseEntity<Void> updateEstablishment(int customerId, int establishmentId) {
        Establishment establishmentById = establishmentConsumer.establishmentById(establishmentId).getBody();
        Establishment establishment = new Establishment();
        establishment.setEstablishmentId(Objects.requireNonNull(establishmentById).getEstablishmentId());
        establishment.setEstablishmentName(establishmentById.getEstablishmentName());
        Set<Establishment> establishmentSet = new HashSet<>();
        establishmentSet.add(establishment);
        Customer customer = new Customer();
        customer.setCustomerId(customerId);
        customer.setEstablishmentList(establishmentSet);
        return customerConsumer.updateEstablishment(customer.getCustomerId(), establishment.getEstablishmentId(), jwtToken);
    }

    public ResponseEntity<Void> updateCustomer(int customerId, String releaseDate) {
        LocalDateTime dateRelease = LocalDateTime.parse(releaseDate, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        Customer customer = customerConsumer.customerById(customerId, jwtToken).getBody();
        Objects.requireNonNull(customer).setReleaseDate(dateRelease);
        return customerConsumer.updateCustomer(customer, jwtToken);
    }

    public ResponseEntity<Void> deleteJuridicProtection(int customerJuridicProtectionId) {
        customerConsumer.findAllCustomers(jwtToken);
        return customerConsumer.deleteJuridicProtection(customerJuridicProtectionId, jwtToken);
    }

    public ResponseEntity<Void> deleteEstablishment(int customerEstablishmentId) {
        customerConsumer.findAllCustomers(jwtToken);
        return customerConsumer.deleteEstablishment(customerEstablishmentId, jwtToken);
    }

    public ResponseEntity<Void> deleteNeeds(int customerNeedsId) {
        customerConsumer.findAllCustomers(jwtToken);
        return customerConsumer.delNeeds(customerNeedsId, jwtToken);
    }

    public ResponseEntity<Void> deleteDirectBenefits(int customerDirectBenefitsId) {
        customerConsumer.findAllCustomers(jwtToken);
        return customerConsumer.delDirectBenefits(customerDirectBenefitsId, jwtToken);
    }

    public ResponseEntity<Void> deleteIndirectNeeds(int customerIndirectBenefitsId) {
        customerConsumer.findAllCustomers(jwtToken);
        return customerConsumer.delIndirectBenefits(customerIndirectBenefitsId, jwtToken);
    }
}
