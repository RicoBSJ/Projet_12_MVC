package com.aubrun.eric.projet12.business.service;

import com.aubrun.eric.projet12.consumer.RoleConsumer;
import com.aubrun.eric.projet12.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RoleService {

    private final RoleConsumer roleConsumer;
    private final JwtToken jwtToken;

    public RoleService(RoleConsumer roleConsumer, JwtToken jwtToken) {
        this.roleConsumer = roleConsumer;
        this.jwtToken = jwtToken;
    }

    public ResponseEntity<Roles> getAllRoles() {
        return roleConsumer.findAllRoles(jwtToken);
    }
}
