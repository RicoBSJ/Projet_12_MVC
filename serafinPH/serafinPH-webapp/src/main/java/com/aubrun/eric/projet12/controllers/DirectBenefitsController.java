package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.service.DirectBenefitsService;
import com.aubrun.eric.projet12.business.service.NomenclatureUserService;
import com.aubrun.eric.projet12.model.DirectsBenefits;
import com.aubrun.eric.projet12.model.NomenclatureUsers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DirectBenefitsController {

    private final DirectBenefitsService directBenefitsService;
    private final NomenclatureUserService nomenclatureUserService;

    public DirectBenefitsController(DirectBenefitsService directBenefitsService, NomenclatureUserService nomenclatureUserService) {
        this.directBenefitsService = directBenefitsService;
        this.nomenclatureUserService = nomenclatureUserService;
    }

    @GetMapping(value = {"/directBenefits", "/getAllDirectBenefits"})
    public String printAllDirectBenefits(ModelMap modelMap) {
        DirectsBenefits directBenefits = directBenefitsService.getAllDirectBenefits().getBody();
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("directBenefits", directBenefits);
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "/customer";
    }
}
