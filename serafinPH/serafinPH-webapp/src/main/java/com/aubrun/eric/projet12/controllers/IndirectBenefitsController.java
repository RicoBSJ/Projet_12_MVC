package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.service.IndirectBenefitsService;
import com.aubrun.eric.projet12.business.service.NomenclatureUserService;
import com.aubrun.eric.projet12.model.IndirectsBenefits;
import com.aubrun.eric.projet12.model.NomenclatureUsers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndirectBenefitsController {

    private final IndirectBenefitsService indirectBenefitsService;
    private final NomenclatureUserService nomenclatureUserService;

    public IndirectBenefitsController(IndirectBenefitsService indirectBenefitsService, NomenclatureUserService nomenclatureUserService) {
        this.indirectBenefitsService = indirectBenefitsService;
        this.nomenclatureUserService = nomenclatureUserService;
    }

    @GetMapping(value = {"/indirectBenefits", "/getAllIndirectBenefits"})
    public String printAllDirectBenefits(ModelMap modelMap) {
        IndirectsBenefits indirectBenefits = indirectBenefitsService.getAllIndirectBenefits().getBody();
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("indirectBenefits", indirectBenefits);
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "/customer";
    }
}
