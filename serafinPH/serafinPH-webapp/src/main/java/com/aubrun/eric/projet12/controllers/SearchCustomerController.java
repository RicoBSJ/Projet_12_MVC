package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.service.CustomerService;
import com.aubrun.eric.projet12.model.Customer;
import com.aubrun.eric.projet12.model.SearchCustomer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class SearchCustomerController {

    private final CustomerService customerService;

    public SearchCustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @ModelAttribute(value = "searchCustomer")
    public SearchCustomer setSearchCustomer(){
        return new SearchCustomer();
    }

    @GetMapping(value = "/searchCustomerForm")
    public String printAllCustomers(ModelMap modelMap){
        modelMap.addAttribute("searchCustomer", new SearchCustomer());
        return "../include/searchCustomerForm";
    }

    @PostMapping(value = "/search")
    private ModelAndView searchCustomerSubmit(@ModelAttribute("searchCustomer") SearchCustomer searchCustomer){
        List<Customer> result = customerService.searchCustomer(searchCustomer).getBody();
        ModelAndView modelAndView = new ModelAndView("home", "searchCustomer", new SearchCustomer());
        modelAndView.addObject("customers", result);
        return modelAndView;
    }
}
