package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.service.CustomerService;
import com.aubrun.eric.projet12.business.service.EstablishmentService;
import com.aubrun.eric.projet12.business.service.NomenclatureUserService;
import com.aubrun.eric.projet12.business.service.RoleService;
import com.aubrun.eric.projet12.config.UserDetailsImpl;
import com.aubrun.eric.projet12.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Objects;

@Controller
@SessionAttributes("nomenclatureUser")
public class NomenclatureUserController {

    private final NomenclatureUserService nomenclatureUserService;
    private final CustomerService customerService;
    private final EstablishmentService establishmentService;
    private final RoleService roleService;

    public NomenclatureUserController(NomenclatureUserService nomenclatureUserService, CustomerService customerService, EstablishmentService establishmentService, RoleService roleService) {
        this.nomenclatureUserService = nomenclatureUserService;
        this.customerService = customerService;
        this.establishmentService = establishmentService;
        this.roleService = roleService;
    }

    @ModelAttribute(value = "nomenclatureUser")
    public NomenclatureUser setNomenclatureUser() {
        return new NomenclatureUser();
    }

    @ModelAttribute(value = "establishment")
    public Establishment setEstablishment(){
        return new Establishment();
    }

    @ModelAttribute(value = "role")
    public Role setRole(){return new Role();}

    @GetMapping("/addEstablishmentUser/{nomenclatureUserId}")
    public String showEstablishment(ModelMap modelMap, @PathVariable("nomenclatureUserId") int nomenclatureUserId){
        Establishments establishments = establishmentService.getAllEstablishments().getBody();
        modelMap.addAttribute("establishments", establishments);
        modelMap.addAttribute("nomenclatureUserId", nomenclatureUserId);
        return "../view/addEstablishmentUser";
    }

    @GetMapping("/addReleaseDateUser/{nomenclatureUserId}")
    public String showCurrentNomenclatureUser(ModelMap modelMap, @PathVariable("nomenclatureUserId") int nomenclatureUserId) {
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        modelMap.addAttribute("nomenclatureUserId", nomenclatureUserId);
        return "../view/addReleaseDateUser";
    }

    @GetMapping("/addRole/{nomenclatureUserId}")
    public String showRole(ModelMap modelMap, @PathVariable("nomenclatureUserId") int nomenclatureUserId){
        Roles roles = roleService.getAllRoles().getBody();
        modelMap.addAttribute("roles", roles);
        modelMap.addAttribute("nomenclatureUserId", nomenclatureUserId);
        return "../view/addRole";
    }

    @GetMapping(value = { "/signInForm"})
    public String showSignIn() {
        return "../view/signInForm";
    }

    @GetMapping("/signUpForm")
    public String showSignUp(ModelMap modelMap) {
        modelMap.addAttribute("nomenclatureUser", new NomenclatureUser());
        return "../view/signUpForm";
    }

    @GetMapping(value = {"/user", "/getAllUser"})
    public String printAllNomenclatureUsers(ModelMap modelMap) {
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "../view/customer";
    }

    @GetMapping(value = {"/detailsUser/{nomenclatureUserId}"})
    public String detailsCustomer(ModelMap modelMap, @PathVariable("nomenclatureUserId") int nomenclatureUserId, Principal principal) {
        NomenclatureUser nomenclatureUser = nomenclatureUserService.getUserById(nomenclatureUserId).getBody();
        modelMap.addAttribute("nomenclatureUser", nomenclatureUser);
        Customers customers = customerService.findCustomers(principal).getBody();
        modelMap.addAttribute("customers", customers);
        return "../view/detailsUser";
    }

    @RequestMapping(value="/loginProcess", method = RequestMethod.POST, headers = {"content-type=application/x-www-form-urlencoded"})
    public ModelAndView login(@RequestParam("username") String email, @RequestParam("password")  String password) {
        ModelAndView modelAndView = new ModelAndView("../view/signInSuccess");
        try {
            ResponseEntity<JwtResponse> result = nomenclatureUserService.login(email, password);
            addPrincipal(Objects.requireNonNull(result.getBody()));
            if (result.getStatusCode().equals(HttpStatus.OK)) {
                modelAndView.addObject("message", "Connexion réussie : ");
            } else {
                modelAndView.addObject("message", result.getBody());
            }
            modelAndView.addObject("username", email);
        } catch (Exception e){
            modelAndView.addObject("error", "Veuillez vérifier vos identifiant/mot de passe !");
        }
        return modelAndView;
    }

    private void addPrincipal(JwtResponse jwt){
        UserDetails userDetails = new UserDetailsImpl(jwt.getUsername(), null, jwt.getRoles());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());
        SecurityContext context = SecurityContextHolder.getContext();
        context.setAuthentication(authentication);
    }

    @PostMapping("/registration")
    public ModelAndView registrationUser(@ModelAttribute("addNomenclatureUser") AddNomenclatureUser addNomenclatureUser) {
        ResponseEntity<NomenclatureUser> user = nomenclatureUserService.addUser(addNomenclatureUser);
        System.out.println("Nom : " + addNomenclatureUser.getLastName());
        System.out.println("Email : " + addNomenclatureUser.getEmail());
        System.out.println("Mot de passe : " + addNomenclatureUser.getPassword());
        System.out.println("Téléphone : " + addNomenclatureUser.getPhoneUser());
        System.out.println("Numéro de SS : " + addNomenclatureUser.getSocialSecurityNumberUser());
        System.out.println("Date de naissance : " + addNomenclatureUser.getDateBirthUser());
        System.out.println("Date d'entrée : " + addNomenclatureUser.getEntryDateUser());
        System.out.println("Age : " + addNomenclatureUser.getAgeUser());
        System.out.println("Id : " + addNomenclatureUser.getNomenclatureUserId());

        ModelAndView modelAndView = new ModelAndView("../view/signUpSuccess");
        modelAndView.addObject("message", "Inscription réussie");
        modelAndView.addObject("lastName", addNomenclatureUser.getLastName());
        modelAndView.addObject(user);
        return modelAndView;
    }

    @PostMapping(value = "/addReleaseDateUser")
    public String updateCustomer(@ModelAttribute("addReleaseDateUser") AddReleaseDateUser addReleaseDateUser, ModelMap modelMap){
        nomenclatureUserService.updateNomenclatureUser(addReleaseDateUser.getNomenclatureUserId(), addReleaseDateUser.getReleaseDate());
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "customer";
    }

    @PostMapping("/addEstablishmentUser")
    public String addEstablishment(@ModelAttribute ("addEstablishmentUser") AddEstablishmentUser addEstablishmentUser, ModelMap modelMap){
        nomenclatureUserService.addEstablishment(addEstablishmentUser.getNomenclatureUserId(), addEstablishmentUser.getEstablishmentId());
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        Establishments establishments = establishmentService.getAllEstablishments().getBody();
        modelMap.addAttribute("establishments", establishments);
        return "customer";
    }

    @PostMapping("/addRole")
    public String addRole(@ModelAttribute("addRole") AddRole addRole, ModelMap modelMap){
        nomenclatureUserService.addRole(addRole.getNomenclatureUserId(), addRole.getRoleId());
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        Roles roles = roleService.getAllRoles().getBody();
        modelMap.addAttribute("roles", roles);
        return "customer";
    }

    @PostMapping(value = "/delEstablishment")
    public String deleteEstablishment(@ModelAttribute("deleteEstablishmentForm") DeleteEstablishmentForm deleteEstablishmentForm, ModelMap modelMap) {
        nomenclatureUserService.deleteEstablishment(deleteEstablishmentForm.getNomenclatureUserEstablishmentId());
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "customer";
    }

    @PostMapping(value = "/delRole")
    public String delRole(@ModelAttribute("deleteRoleForm") DeleteRoleForm deleteRoleForm, ModelMap modelMap){
        nomenclatureUserService.deleteRole(deleteRoleForm.getNomenclatureUserRoleId());
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "customer";
    }

    @PostMapping("/deconnect")
    public String leave(HttpSession session, SessionStatus status) {
        status.setComplete();
        session.removeAttribute("username");
        session.invalidate();
        nomenclatureUserService.logout();
        return "redirect:/signInForm";
    }
}
