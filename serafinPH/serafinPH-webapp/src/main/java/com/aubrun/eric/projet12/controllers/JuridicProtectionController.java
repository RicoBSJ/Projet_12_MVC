package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.service.JuridicProtectionService;
import com.aubrun.eric.projet12.business.service.NomenclatureUserService;
import com.aubrun.eric.projet12.model.JuridicProtections;
import com.aubrun.eric.projet12.model.NomenclatureUsers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class JuridicProtectionController {

    private final JuridicProtectionService juridicProtectionService;
    private final NomenclatureUserService nomenclatureUserService;

    public JuridicProtectionController(JuridicProtectionService juridicProtectionService, NomenclatureUserService nomenclatureUserService) {
        this.juridicProtectionService = juridicProtectionService;
        this.nomenclatureUserService = nomenclatureUserService;
    }

    @GetMapping(value = {"/juridicProtection", "/getAllJuridicProtection"})
    public String printAllJuridicProtections(ModelMap modelMap) {
        JuridicProtections juridicProtections = juridicProtectionService.getAllJuridicProtection().getBody();
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        modelMap.addAttribute("juridicProtections", juridicProtections);
        return "/customer";
    }
}
