package com.aubrun.eric.projet12.config;

import com.aubrun.eric.projet12.business.service.NomenclatureUserService;
import com.aubrun.eric.projet12.model.JwtResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final NomenclatureUserService nomenclatureUserService;

    public CustomAuthenticationProvider(NomenclatureUserService nomenclatureUserService) {
        this.nomenclatureUserService = nomenclatureUserService;
    }

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
        ResponseEntity<JwtResponse> result = nomenclatureUserService.login(username, password);
        return addPrincipal(Objects.requireNonNull(result.getBody()));
    }

    @Override
    public boolean supports(Class<?> arg0) {
        return true;
    }

    private UsernamePasswordAuthenticationToken addPrincipal(JwtResponse jwt) {
        UserDetails userDetails = new UserDetailsImpl(jwt.getUsername(), null, jwt.getRoles());
        return new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());
    }

} 