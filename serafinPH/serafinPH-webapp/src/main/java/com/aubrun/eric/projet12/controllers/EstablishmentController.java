package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.service.EstablishmentService;
import com.aubrun.eric.projet12.business.service.NomenclatureUserService;
import com.aubrun.eric.projet12.model.Establishments;
import com.aubrun.eric.projet12.model.NomenclatureUsers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EstablishmentController {

    private final EstablishmentService establishmentService;
    private final NomenclatureUserService nomenclatureUserService;

    public EstablishmentController(EstablishmentService establishmentService, NomenclatureUserService nomenclatureUserService) {
        this.establishmentService = establishmentService;
        this.nomenclatureUserService = nomenclatureUserService;
    }

    @GetMapping(value = {"/establishments", "/getAllEstablishments"})
    public String printAllDirectBenefits(ModelMap modelMap) {
        Establishments establishments = establishmentService.getAllEstablishments().getBody();
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("establishments", establishments);
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "/customer";
    }
}
