package com.aubrun.eric.projet12.controllers;

import com.aubrun.eric.projet12.business.service.NeedsService;
import com.aubrun.eric.projet12.business.service.NomenclatureUserService;
import com.aubrun.eric.projet12.model.Need;
import com.aubrun.eric.projet12.model.NomenclatureUsers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NeedsController {

    private final NeedsService needsService;
    private final NomenclatureUserService nomenclatureUserService;

    public NeedsController(NeedsService needsService, NomenclatureUserService nomenclatureUserService) {
        this.needsService = needsService;
        this.nomenclatureUserService = nomenclatureUserService;
    }

    @GetMapping(value = {"/needs", "/getAllNeeds"})
    public String printAllNeeds(ModelMap modelMap) {
        Need need = needsService.getAllNeeds().getBody();
        NomenclatureUsers nomenclatureUsers = nomenclatureUserService.getAllUsers().getBody();
        modelMap.addAttribute("need", need);
        modelMap.addAttribute("nomenclatureUsers", nomenclatureUsers);
        return "/customer";
    }
}
