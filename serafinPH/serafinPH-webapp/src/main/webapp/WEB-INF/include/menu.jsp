<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
          crossorigin="anonymous">
    <title>Rico's SERAFIN PH@Project</title>
</head>
<body>
<div id="menu" class="container p-3 user-select-none sticky-top">
    <nav class="navbar navbar-expand-lg navbar-light bg-success">
        <a class="navbar-brand fw-bolder text-uppercase text-end">Rico's SERAFIN PH@Project</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <c:if test="${!empty pageContext.request.userPrincipal.name}">
                    <li class="nav-item align-middle text-center">
                        <a class="nav-link text-light">${pageContext.request.userPrincipal.name} est connecté</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link bg-info" href="<c:url value="customer" />">Espace utilisateur</a>
                    </li>
                    <li class="nav-item">
                        <form:form action="${pageContext.request.contextPath}/deconnect" method="post">
                            <input class="nav-link" type="submit" value="Déconnexion"/>
                        </form:form>
                    </li>
                </c:if>
            </ul>
        </div>
    </nav>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy"
        crossorigin="anonymous"></script>
</body>
</html>