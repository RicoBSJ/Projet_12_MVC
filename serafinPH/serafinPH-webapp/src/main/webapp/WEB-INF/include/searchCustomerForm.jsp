<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
          crossorigin="anonymous">
    <title>Rico's SERAFIN PH@Project</title>
</head>
<body>
<div class="container">
    <form:form modelAttribute="searchCustomer" action="${pageContext.request.contextPath}/search" method="post">
        <br/>
        <p>Vous pouvez sélectionner vos critères de recherche via ce formulaire.</p>

        <div class="form-group row">
            <div class="col-sm-6 p-3">
                <form:label path="nomenclatureUser">Recherche d'usager par référent</form:label>
                <form:input path="nomenclatureUser" placeholder="Entrer le référent"/>
            </div>
            <div class="col-sm-6 p-3">
                <form:label path="customerFirstName">Recherche d'usager par prénom</form:label>
                <form:input path="customerFirstName" placeholder="Entrer le prénom"/>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6 p-3">
                <form:label path="customerLastName">Recherche d'usager par nom</form:label>
                <form:input path="customerLastName" placeholder="Entrer le nom"/>
            </div>
            <div class="col-sm-6 p-3">
                <form:label path="age">Recherche d'usager par âge</form:label>
                <form:input path="age" placeholder="Entrer l'âge'"/>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6 p-3">
                <form:label path="entryDate">Recherche d'usager par date d'entrée</form:label>
                <form:input path="entryDate" placeholder="Entrer la date d'entrée"/>
            </div>
            <div class="col-sm-6 p-3">
                <form:label path="dateBirth">Recherche d'usager par date de naissance</form:label>
                <form:input path="dateBirth" placeholder="Entrer la date de naissance"/>
            </div>
        </div>
        <input type="submit" value="Valider">
    </form:form>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy"
        crossorigin="anonymous"></script>
</body>
</html>
