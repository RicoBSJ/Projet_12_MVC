<%--
  Created by IntelliJ IDEA.
  User: ricobsj
  Date: 24/12/2020
  Time: 16:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
          crossorigin="anonymous">
    <title>Page de connexion</title>
</head>
<body>
<div class="container bg-light p-5">
    <form modelattribute="addNomenclatureUser" method="post" action="${pageContext.request.contextPath}/registration">
        <h1 class="p-1 text-uppercase bg-warning">Formulaire d'inscription</h1>
        <p class="p-5">Vous pouvez vous inscrire via ce formulaire.</p>
        <p>
            <a href="<c:url value="/"/>">Retour à l'accueil</a>
        </p>
        <p>
            <a href="<c:url value="customer"/>">Retour à l'espace utilisateur</a>
        </p>
        <br/>
        <div class="form-group row p-5">
            <div class="col-sm-6 text-center">
                <label for="lastName">Nom<span class="requis">*</span></label>
                <input type="text" id="lastName" name="lastName" placeholder="Entrez votre nom"/>
                <br/>
            </div>
            <div class="col-sm-6 text-center">
                <label for="firstName">Prénom<span class="requis">*</span></label>
                <input type="text" id="firstName" name="firstName" placeholder="Entrez votre prénom"/>
                <br/>
            </div>
        </div>
        <div class="form-group row p-5">
            <div class="col-sm-6 text-center">
                <label for="phoneUser">Numéro de téléphone<span class="requis">*</span></label>
                <input type="tel" id="phoneUser" name="phoneUser" placeholder="Entrez votre numéro de téléphone"/>
                <br/>
            </div>
            <div class="col-sm-6 text-center">
                <label for="socialSecurityNumberUser">Numéro de SS<span class="requis">*</span></label>
                <input type="text" id="socialSecurityNumberUser" name="socialSecurityNumberUser" placeholder="Entrez votre numéro de SS"/>
                <br/>
            </div>
        </div>
        <div class="form-group row p-5">
            <div class="col-sm-6 text-center">
                <label for="dateBirthUser">Date de naissance<span class="requis">*</span></label>
                <input type="date" id="dateBirthUser" name="dateBirthUser" placeholder="Entrez votre date de naissance"/>
                <br/>
            </div>
            <div class="col-sm-6 text-center">
                <label for="entryDateUser">Date d'entrée<span class="requis">*</span></label>
                <input type="datetime-local" id="entryDateUser" name="entryDateUser" placeholder="Entrez votre date d'entrée"/>
                <br/>
            </div>
        </div>
        <div class="form-group row p-5">
            <div class="col-sm-6 text-center">
                <label for="ageUser">Âge<span class="requis">*</span></label>
                <input type="text" id="ageUser" name="ageUser" placeholder="Entrez votre âge"/>
                <br/>
            </div>
            <div class="col-sm-6 text-center">
                <label for="password">Mot de passe<span class="requis">*</span></label>
                <input type="password" id="password" name="password" placeholder="Entrez votre mot de passe"/>
                <br/>
            </div>
        </div>
        <div class="form-group row p-5">
            <div class="col-sm-6 text-center">
                <label for="email">Email<span class="requis">*</span></label>
                <input type="email" id="email" name="email" placeholder="Entrez votre email"/>
                <br/>
            </div>
        </div>
        <form:hidden path="nomenclatureUser.nomenclatureUserId" value="1"/>
        <br/>
        <input type="submit" value="Inscription"/>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy"
        crossorigin="anonymous"></script>
</body>
</html>
