<%--
  Created by IntelliJ IDEA.
  User: ricobsj
  Date: 24/04/2022
  Time: 01:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
          crossorigin="anonymous">
    <title>Espace utilisateur</title>
</head>
<body>
<div class="container user-select-none">
    <br/>
    <h1 class="bg-info text-center sticky-top">Espace utilisateur de "${pageContext.request.userPrincipal.name}"</h1>
    <br/>
    <p>
        Role(s) de l'utilisateur connecté: <sec:authentication property="principal.authorities"/>
    </p>
    <p>
        <a href="<c:url value="/"/>">Retour à l'accueil</a>
    </p>
    <br/>
    <sec:authorize access="hasRole('ROLE_USER')">
        <br/>
        <h3>Ajout ou suppression de la Protection juridique et de l'Etablissement de l'usager</h3>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Référent</th>
                    <th>Protection Juridique</th>
                    <th>Etablissement</th>
                    <th>Détails de l'usager</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="customer" items="${customers}">
                    <c:if test="${customer.releaseDate == null}">
                    <tr class="align-middle text-center">
                        <td>${customer.customerLastName}</td>
                        <td>${customer.customerFirstName}</td>
                        <td>${customer.nomenclatureUser}</td>
                        <td>
                            <c:forEach var="juridicProtection" items="${customer.juridicProtectionList}">
                                ${juridicProtection.juridicProtectionName}
                                <br/>
                            </c:forEach>
                            <br/>
                            <br/>
                            <form modelAttribute="deleteJuridicProtectionForm"
                                  action="${pageContext.request.contextPath}/deleteJuridicProtection" method="post">
                                <input type="hidden" id="customerJuridicProtectionId" name="customerJuridicProtectionId"
                                       value="${customer.customerId}"/>
                                <input type="submit" name="tag" value="Supprimer la protection juridique"/>
                            </form>
                            <br/>
                            <label for="${customer.customerId}">
                                <a class="col-sm-6 p-3 bg-primary text-center text-white"
                                   href="<c:url value="addJuridicProtection/${customer.customerId}" />">Ajout d'une protection juridique</a>
                            </label>
                            <br/>
                            <br/>
                        </td>
                        <td>
                            <c:forEach var="establishment" items="${customer.establishmentList}">
                                ${establishment.establishmentName}
                                <br/>
                            </c:forEach>
                            <br/>
                            <br/>
                            <form modelAttribute="deleteEstablishmentForm"
                                  action="${pageContext.request.contextPath}/deleteEstablishment" method="post">
                                <input type="hidden" id="customerEstablishmentId" name="customerEstablishmentId"
                                       value="${customer.customerId}"/>
                                <input type="submit" name="tag" value="Supprimer l'établissement"/>
                            </form>
                            <br/>
                            <label for="${customer.customerId}">
                                <a class="col-sm-6 p-3 bg-primary text-center text-white"
                                   href="<c:url value="addEstablishmentCustomer/${customer.customerId}" />">Ajout d'un établissement</a>
                            </label>
                            <br/>
                            <br/>
                        </td>
                        <td>
                            <label for="${customer.customerId}">
                                <a class="col-sm-6 p-3 bg-primary text-center text-white" href="<c:url value="detailsCustomer/${customer.customerId}" />">Détails de l'usager</a>
                            </label>
                        </td>
                    </tr>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <br/>
        <h3>Ajout et suppression des besoins de l'usager</h3>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Référent</th>
                    <th>Besoins</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="customer" items="${customers}">
                    <c:if test="${customer.releaseDate == null}">
                    <tr class="align-middle text-center">
                        <td>${customer.customerLastName}</td>
                        <td>${customer.customerFirstName}</td>
                        <td>${customer.nomenclatureUser}</td>
                        <td>
                            <c:forEach var="needs" items="${customer.needsList}">
                                ${needs.needName}
                                <br/>
                            </c:forEach>
                            <br/>
                            <br/>
                            <form modelAttribute="deleteNeedsForm"
                                  action="${pageContext.request.contextPath}/delNeed" method="post">
                                <input type="hidden" id="customerNeedsId" name="customerNeedsId"
                                       value="${customer.customerId}"/>
                                <input type="submit" name="tag" value="Supprimer les besoins"/>
                            </form>
                            <br/>
                            <label for="${customer.customerId}">
                                <a class="col-sm-6 p-3 bg-primary text-center text-white"
                                   href="<c:url value="addNeeds/${customer.customerId}" />">Ajout d'un besoin</a>
                            </label>
                            <br/>
                            <br/>
                        </td>
                    </tr>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <br/>
        <h3>Ajout et suppression des besoins directs et indirects de l'usager</h3>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Référent</th>
                    <th>Besoins directs</th>
                    <th>Besoins indirects</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="customer" items="${customers}">
                    <c:if test="${customer.releaseDate == null}">
                    <tr class="align-middle text-center">
                        <td>${customer.customerLastName}</td>
                        <td>${customer.customerFirstName}</td>
                        <td>${customer.nomenclatureUser}</td>
                        <td>
                            <c:forEach var="directBenefits" items="${customer.directBenefitsList}">
                                ${directBenefits.directBenefitName}
                                <br/>
                            </c:forEach>
                            <br/>
                            <br/>
                            <form modelAttribute="deleteDirectBenefitsForm"
                                  action="${pageContext.request.contextPath}/delDirectBenefits" method="post">
                                <input type="hidden" id="customerDirectBenefitsId" name="customerDirectBenefitsId"
                                       value="${customer.customerId}"/>
                                <input type="submit" name="tag" value="Supprimer les besoins directs"/>
                            </form>
                            <br/>
                            <label for="${customer.customerId}">
                                <a class="col-sm-6 p-3 bg-primary text-center text-white"
                                   href="<c:url value="addDirectBenefits/${customer.customerId}" />">Ajout d'un besoin direct</a>
                            </label>
                            <br/>
                            <br/>
                        </td>
                        <td>
                            <c:forEach var="indirectBenefit" items="${customer.indirectBenefitsList}">
                                ${indirectBenefit.indirectBenefitName}
                                <br/>
                            </c:forEach>
                            <br/>
                            <br/>
                            <form modelAttribute="deleteIndirectBenefitsForm"
                                  action="${pageContext.request.contextPath}/delIndirectBenefits" method="post">
                                <input type="hidden" id="customerIndirectBenefitsId" name="customerIndirectBenefitsId"
                                       value="${customer.customerId}"/>
                                <input type="submit" name="tag" value="Supprimer les besoins indirects"/>
                            </form>
                            <br/>
                            <label for="${customer.customerId}">
                                <a class="col-sm-6 p-3 bg-primary text-center text-white"
                                   href="<c:url value="addIndirectBenefits/${customer.customerId}" />">Ajout d'un besoin indirect</a>
                            </label>
                            <br/>
                            <br/>
                        </td>
                    </tr>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_MODERATOR')">
        <br/>
        <h3>Changement du référent et création d'un nouvel usager</h3>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Référent</th>
                    <th>Sortir l'usager</th>
                    <th>Détails de l'usager</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="customer" items="${customers}">
                    <c:if test="${customer.releaseDate == null}">
                        <c:if test="${customer.nomenclatureUser != ''}">
                    <tr class="align-middle text-center">
                        <td>${customer.customerLastName}</td>
                        <td>${customer.customerFirstName}</td>
                        <td>
                            ${customer.nomenclatureUser}
                            <br/>
                            <br/>
                            <label for="${customer.customerId}">
                                <a class="col-sm-6 p-3 bg-primary text-center text-white"
                                   href="<c:url value="addNomenclatureUser/${customer.customerId}" />">Changer l'utilisateur</a>
                            </label>
                        </td>
                        <td>
                            <label for="${customer.customerId}">
                                <a class="col-sm-6 p-3 bg-primary text-center text-white" href="<c:url value="addReleaseDate/${customer.customerId}" />">Sortir l'usager</a>
                            </label>
                        </td>
                        <td>
                            <label for="${customer.customerId}">
                                <a class="col-sm-6 p-3 bg-primary text-center text-white" href="<c:url value="detailsCustomer/${customer.customerId}" />">Détails de l'usager</a>
                            </label>
                        </td>
                    </tr>
                        </c:if>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <p>
            <a class="col-sm-6 p-3 bg-primary text-center text-white" href="<c:url value="addCustomer/${nomenclatureUser.nomenclatureUserId}" />">Création d'un
                nouvel usager</a>
        </p>
        <br/>
        <h3>Usagers sortis</h3>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Date de sortie</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="customer" items="${customers}">
                    <c:if test="${customer.releaseDate != null}">
                        <c:if test="${customer.nomenclatureUser == ''}">
                        <tr class="align-middle text-center">
                            <td>${customer.customerLastName}</td>
                            <td>${customer.customerFirstName}</td>
                            <td>${customer.releaseDate}</td>
                        </tr>
                        </c:if>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <br/>
        <h3>Attribution d'un nouveau référent pour les usagers</h3>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Référent</th>
                    <th>Sortir l'usager</th>
                    <th>Détails de l'usager</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="customer" items="${customers}">
                    <c:if test="${customer.releaseDate == null}">
                        <c:if test="${customer.nomenclatureUser == ''}">
                            <tr class="align-middle text-center">
                                <td>${customer.customerLastName}</td>
                                <td>${customer.customerFirstName}</td>
                                <td>
                                        ${customer.nomenclatureUser}
                                    <br/>
                                    <br/>
                                    <label for="${customer.customerId}">
                                        <a class="col-sm-6 p-3 bg-primary text-center text-white"
                                           href="<c:url value="addNomenclatureUser/${customer.customerId}" />">Attribution d'un utilisateur</a>
                                    </label>
                                </td>
                                <td>
                                    <label for="${customer.customerId}">
                                        <a class="col-sm-6 p-3 bg-primary text-center text-white" href="<c:url value="addReleaseDate/${customer.customerId}" />">Sortir l'usager</a>
                                    </label>
                                </td>
                                <td>
                                    <label for="${customer.customerId}">
                                        <a class="col-sm-6 p-3 bg-primary text-center text-white" href="<c:url value="detailsCustomer/${customer.customerId}" />">Détails de l'usager</a>
                                    </label>
                                </td>
                            </tr>
                        </c:if>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <br/>
        <h3>Ajout ou suppression du rôle ou de l'établissement d'un utilisateur</h3>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th>Nom</th>
                    <th>Role(s)</th>
                    <th>Etablissement</th>
                    <th>Détails de l'utilisateur</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="nomenclatureUser" items="${nomenclatureUsers}">
                    <c:if test="${nomenclatureUser.releaseDateUser == null}">
                    <tr class="align-middle text-center">
                        <td>${nomenclatureUser.lastName}</td>
                        <td>
                            <c:forEach var="role" items="${nomenclatureUser.roles}">
                                ${role.roleName}
                                <br/>
                            </c:forEach>
                            <br/>
                            <br/>
                            <form modelattribute="deleteRoleForm"
                                  action="${pageContext.request.contextPath}/delRole" method="post">
                                <input type="hidden" id="nomenclatureUserRoleId" name="nomenclatureUserRoleId"
                                       value="${nomenclatureUser.nomenclatureUserId}"/>
                                <input type="submit" name="tag" value="Supprimer le rôle">
                            </form>
                            <br/>
                            <label for="${nomenclatureUser.nomenclatureUserId}">
                                <a class="col-sm-6 p-1 bg-primary text-center text-white"
                                   href="<c:url value="addRole/${nomenclatureUser.nomenclatureUserId}" />">Ajout d'un role</a>
                            </label>
                            <br/>
                            <br/>
                        </td>
                        <td>
                            <c:forEach var="establishment" items="${nomenclatureUser.establishments}">
                                ${establishment.establishmentName}
                                <br/>
                            </c:forEach>
                            <br/>
                            <br/>
                            <form modelAttribute="deleteEstablishmentForm"
                                  action="${pageContext.request.contextPath}/delEstablishment" method="post">
                                <input type="hidden" id="nomenclatureUserEstablishmentId" name="nomenclatureUserEstablishmentId"
                                       value="${nomenclatureUser.nomenclatureUserId}"/>
                                <input type="submit" name="tag" value="Supprimer l'établissement"/>
                            </form>
                            <br/>
                            <label for="${nomenclatureUser.nomenclatureUserId}">
                                <a class="col-sm-6 p-1 bg-primary text-center text-white"
                                   href="<c:url value="addEstablishmentUser/${nomenclatureUser.nomenclatureUserId}" />">Ajout d'un établissement</a>
                            </label>
                            <br/>
                            <br/>
                        </td>
                        <td>
                            <a class="col-sm-6 p-3 bg-primary text-center text-white" href="<c:url value="detailsUser/${nomenclatureUser.nomenclatureUserId}" />">Détails de l'utilisateur</a>
                        </td>
                    </tr>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <p>
            <a class="col-sm-6 p-3 bg-primary text-center text-white" href="<c:url value="signUpForm" />">Création d'un
                nouvel utilisateur</a>
        </p>
        <h3>Sortie d'un utilisateur</h3>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th>Nom</th>
                    <th>Sortir l'utilisateur</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="nomenclatureUser" items="${nomenclatureUsers}">
                    <c:if test="${nomenclatureUser.releaseDateUser == null}">
                        <tr class="align-middle text-center">
                            <td>${nomenclatureUser.lastName}</td>
                            <td>
                                <label for="${nomenclatureUser.nomenclatureUserId}">
                                    <a class="col-sm-6 p-3 bg-primary text-center text-white" href="<c:url value="addReleaseDateUser/${nomenclatureUser.nomenclatureUserId}" />">Sortir l'utilisateur</a>
                                </label>
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <br/>
        <h3>Utilisateurs sortis</h3>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th>Nom</th>
                    <th>Date de sortie</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="nomenclatureUser" items="${nomenclatureUsers}">
                    <c:if test="${nomenclatureUser.releaseDateUser != null}">
                        <tr class="align-middle text-center">
                            <td>${nomenclatureUser.lastName}</td>
                            <td>${nomenclatureUser.releaseDateUser}</td>
                        </tr>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </sec:authorize>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy"
        crossorigin="anonymous"></script>
</body>
</html>