<%--
  Created by IntelliJ IDEA.
  User: ricobsj
  Date: 27/12/2020
  Time: 00:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
          crossorigin="anonymous">
    <title>Connexion</title>
</head>
<body>
<div class="container bg-light p-5">
    <c:if test="${empty error}">
    <form>
        <h1 class="p-1 text-uppercase bg-success">Réussite de la connexion</h1>
        <p class="p-5">Vous êtes correctement connecté et identifié.</p>
            <div class="form-group row">
                <div class="col-sm-6">
                <br/>
                <table class="table table-striped table-bordered">
                    <tr>
                        <td><b>Name </b>: ${pageContext.request.userPrincipal.name}</td>
                    </tr>
                </table>
                <br/>
                <p>
                    <a> ${pageContext.request.userPrincipal.name} est bien connecté</a>
                </p>
                <br/>
                <p>
                    <a href="<c:url value="/"/>">Page d'accueil</a>
                </p>
                <br/>
                </div>
            </div>
        <br/>
        </form>
        </c:if>
        <c:if test="${not empty error}">
        <form>
            <h1 class="p-1 text-uppercase bg-danger">Echec de la connexion</h1>
            <p class="p-5">Votre connexion a échoué.</p>
            <div class="form-group row">
                <div class="col-sm-6">
                <table class="table table-striped table-bordered">
                    <tr>
                        <td><b>Détails de l'erreur </b>: ${error}</td>
                    </tr>
                </table>
                <br/>
                <p>
                    <a href="<c:url value="/"/>">Retour à la page de connexion</a>
                </p>
                <br/>
                </div>
            </div>
            <br/>
        </form>
        </c:if>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy"
        crossorigin="anonymous"></script>
</body>
</html>
