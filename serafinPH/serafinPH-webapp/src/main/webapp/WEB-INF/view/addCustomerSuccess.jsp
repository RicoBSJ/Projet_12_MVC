<%--
  Created by IntelliJ IDEA.
  User: ricobsj
  Date: 08/07/2022
  Time: 19:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
    <title>Création d'un usager</title>
</head>
<body>
<div class="container bg-light p-5">
    <form>
        <h1 class="p-1 text-uppercase bg-success">Réussite de la création d'un usager</h1>
        <p class="p-5">L'usager a correctement été créé.</p>
        <div class="form-group row">
            <div class="col-sm-6">
                <br/>
                <table class="table table-striped table-bordered">
                    <tr>
                        <td><b>Name </b>: ${customer.customerLastName}</td>
                    </tr>
                </table>
                <br/>
                <p>
                    <a> ${customer.customerLastName} est bien enregistré</a>
                </p>
                <br/>
                <p>
                    <a class="col-sm-6 p-3 text-center bg-light" href="<c:url value="customer" />">Retour à l'espace utilisateur</a>
                <p/>
                <br/>
                <p>
                    <a class="col-sm-6 p-3 text-center bg-light" href="<c:url value="/"/>">Retour à l'accueil</a>
                </p>
                <br/>
            </div>
        </div>
        <br/>
    </form>
</div>
</body>
</html>
