<%--
  Created by IntelliJ IDEA.
  User: AUBRUNE
  Date: 08/12/2020
  Time: 15:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
          crossorigin="anonymous">
    <title>Rico's SERAFIN PH@Project</title>
</head>
<body>
<div class="container user-select-none">
    <c:import url="../include/menu.jsp"/>
    <br/>
    <br/>
    <br/>
    <section class="bg-info text text-center">
        <h2>Usagers</h2>
    </section>
    <br/>
    <div class="table-responsive">
        <table class="table table-responsive-lg table-fluid table-striped p-5" id="myTable">
            <thead>
            <tr class="table-info align-middle text-center">
                <th>Nom</th>
                <th>Prénom</th>
                <th>Référent</th>
                <th>Date de naissance</th>
                <th>Numéro de SS</th>
                <th>Mutuelle</th>
                <th>Date d'entrée</th>
                <th>Age</th>
                <th>Protection</th>
                <th>Service</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="customer" items="${customers}">
                <c:if test="${customer.releaseDate == null}">
                <tr class="align-middle text-center">
                    <td>${customer.customerLastName}</td>
                    <td>${customer.customerFirstName}</td>
                    <td>${customer.nomenclatureUser}</td>
                    <td>${customer.dateBirth}</td>
                    <td>${customer.socialSecurityNumber}</td>
                    <td>${customer.mutualName}</td>
                    <td>${customer.entryDate}</td>
                    <td>${customer.age}</td>
                    <td>
                        <c:forEach items="${customer.juridicProtectionList}" var="juridicProtection">
                            <c:out value="${juridicProtection.juridicProtectionName}"/>
                        </c:forEach>
                    </td>
                    <td>
                        <c:forEach items="${customer.establishmentList}" var="establishment">
                            <c:out value="${establishment.establishmentName}"/>
                        </c:forEach>
                    </td>
                </tr>
                </c:if>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <footer class="bg-info text-white p-3 text-end">
        <div class="container">
            © Rico's SERAFIN PH@Project
        </div>
    </footer>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy"
        crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>
</body>
</html>