<%--
  Created by IntelliJ IDEA.
  User: AUBRUNE
  Date: 01/09/2022
  Time: 18:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
          crossorigin="anonymous">
    <title>Page d'ajout d'un besoin indirect</title>
</head>
<body>
<div class="container bg-light p-5">
    <form modelattribute="addIndirectBenefits" method="post"
          action="${pageContext.request.contextPath}/addIndirectBenefits">
        <h1 class="p-1 text-uppercase bg-danger">Formulaire d'ajout d'un besoin indirect</h1>
        <p class="p-5">Vous pouvez ajouter un besoin indirect via ce formulaire.</p>
        <p>
            <a href="<c:url value="/"/>">Retour à l'accueil</a>
        </p>
        <p>
            <a href="<c:url value="/customer"/>">Retour à l'espace utilisateur</a>
        </p>
        <br/>
        <input name="customerId" value="${customerId}" type="hidden">
        <div class="form-group row p-5">
            <div class="col-sm-6 text-center">
                <label for="indirectBenefitsId" class="sr-only">Nom du besoin indirect</label>
                <br/>
                <br/>
                <select class="form-control" name="indirectBenefitsId" required autofocus>
                    <option  id="indirectBenefitsId" value="" >Besoin indirect</option>
                    <c:forEach var="indirectBenefits" items="${indirectsBenefits}">
                        <option value="${indirectBenefits.indirectBenefitsId}">${indirectBenefits.indirectBenefitName}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <form:hidden path="customer.customerId" value="1"/>
        <br/>
        <input type="submit" value="Rajout d'un besoin indirect"/>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy"
        crossorigin="anonymous"></script>
</body>
</html>