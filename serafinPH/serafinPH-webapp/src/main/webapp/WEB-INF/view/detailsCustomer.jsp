<%--
  Created by IntelliJ IDEA.
  User: ricobsj
  Date: 04/09/2022
  Time: 15:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
          crossorigin="anonymous">
    <title>Page de détails d'un usager</title>
</head>
<body>
<div class="container bg-light p-5">
    <form modelattribute="customer" method="get" action="detailsCustomer/${customer.customerId}">
        <h1 class="p-1 text-uppercase bg-info">Page de détails
            de ${customer.customerLastName} ${customer.customerFirstName}</h1>
        <p class="p-5">Vous pouvez accéder aux détails d'un usager via cette page.</p>
        <p>
            <a href="<c:url value="/"/>">Retour à l'accueil</a>
        </p>
        <p>
            <a href="<c:url value="/customer"/>">Retour à l'espace utilisateur</a>
        </p>
        <br/>
        <div class="table-responsive">
            <table class="table table-responsive-lg table-fluid table-hover table-sm table-bordered">
                <thead>
                <tr class="table-success text-center">
                    <th scope="col">NOM</th>
                    <th scope="col">PRENOM</th>
                    <th scope="col">REFERENT</th>
                    <th scope="col">DATE DE NAISSANCE</th>
                    <th scope="col">NUMERO DE SECURITE SOCIALE</th>
                    <th scope="col">MUTUELLE</th>
                    <th scope="col">DATE D'ENTREE</th>
                    <th scope="col">ÂGE</th>
                    <th scope="col">PROTECTION JURIDIQUE</th>
                    <th scope="col">ETABLISSEMENT(S)</th>
                    <th scope="col">BESOINS</th>
                    <th scope="col">BESOINS DIRECTS</th>
                    <th scope="col">BESOINS INDIRECTS</th>
                </tr>
                </thead>
                <tbody>
                <tr class="align-middle text-center">
                    <td>${customer.customerLastName}</td>
                    <td>${customer.customerFirstName}</td>
                    <td>${customer.nomenclatureUser}</td>
                    <td>${customer.dateBirth}</td>
                    <td>${customer.socialSecurityNumber}</td>
                    <td>${customer.mutualName}</td>
                    <td>${customer.entryDate}</td>
                    <td>${customer.age}</td>
                    <td>
                        <c:forEach var="juridicProtection" items="${customer.juridicProtectionList}">
                            <p>${juridicProtection.juridicProtectionName}</p>
                        </c:forEach>
                    </td>
                    <td>
                        <c:forEach var="establishment" items="${customer.establishmentList}">
                            <p>${establishment.establishmentName}</p>
                        </c:forEach>
                    </td>
                    <td>
                        <c:forEach var="needs" items="${customer.needsList}">
                            <p>${needs.needName}</p>
                        </c:forEach>
                    </td>
                    <td>
                        <c:forEach var="directBenefits" items="${customer.directBenefitsList}">
                            <p>${directBenefits.directBenefitName}</p>
                        </c:forEach>
                    </td>
                    <td>
                        <c:forEach var="indirectBenefits" items="${customer.indirectBenefitsList}">
                            <p>${indirectBenefits.indirectBenefitName}</p>
                        </c:forEach>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <form:hidden path="customer.customerId" value="1"/>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js"
        integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy"
        crossorigin="anonymous"></script>
</body>
</html>