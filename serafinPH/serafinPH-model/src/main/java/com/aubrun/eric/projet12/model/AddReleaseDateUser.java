package com.aubrun.eric.projet12.model;

public class AddReleaseDateUser {

    private Integer nomenclatureUserId;
    private String releaseDate;

    public Integer getNomenclatureUserId() {
        return nomenclatureUserId;
    }

    public void setNomenclatureUserId(Integer nomenclatureUserId) {
        this.nomenclatureUserId = nomenclatureUserId;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
}
