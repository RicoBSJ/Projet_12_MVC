package com.aubrun.eric.projet12.model;

public class DeleteEstablishmentForm {

    private Integer customerEstablishmentId;
    private Integer nomenclatureUserEstablishmentId;

    public Integer getCustomerEstablishmentId() {
        return customerEstablishmentId;
    }

    public void setCustomerEstablishmentId(Integer customerEstablishmentId) {
        this.customerEstablishmentId = customerEstablishmentId;
    }

    public Integer getNomenclatureUserEstablishmentId() {
        return nomenclatureUserEstablishmentId;
    }

    public void setNomenclatureUserEstablishmentId(Integer nomenclatureUserEstablishmentId) {
        this.nomenclatureUserEstablishmentId = nomenclatureUserEstablishmentId;
    }
}
