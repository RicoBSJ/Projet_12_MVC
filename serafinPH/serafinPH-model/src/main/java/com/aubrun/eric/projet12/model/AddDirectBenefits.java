package com.aubrun.eric.projet12.model;

public class AddDirectBenefits {

    private Integer directBenefitsId;
    private Integer customerId;

    public Integer getDirectBenefitsId() {
        return directBenefitsId;
    }

    public void setDirectBenefitsId(Integer directBenefitsId) {
        this.directBenefitsId = directBenefitsId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
}
