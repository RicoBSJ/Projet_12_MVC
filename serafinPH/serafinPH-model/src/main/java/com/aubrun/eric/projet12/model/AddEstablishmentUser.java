package com.aubrun.eric.projet12.model;

public class AddEstablishmentUser {

    private Integer establishmentId;
    private Integer nomenclatureUserId;

    public Integer getEstablishmentId() {
        return establishmentId;
    }

    public void setEstablishmentId(Integer establishmentId) {
        this.establishmentId = establishmentId;
    }

    public Integer getNomenclatureUserId() {
        return nomenclatureUserId;
    }

    public void setNomenclatureUserId(Integer nomenclatureUserId) {
        this.nomenclatureUserId = nomenclatureUserId;
    }
}
