package com.aubrun.eric.projet12.model;

public class DirectBenefits {

    private Integer directBenefitsId;
    private String directBenefitName;
    private EDirectBenefits directBenefitType;

    public DirectBenefits() {
    }

    public Integer getDirectBenefitsId() {
        return directBenefitsId;
    }

    public void setDirectBenefitsId(Integer directBenefitsId) {
        this.directBenefitsId = directBenefitsId;
    }

    public String getDirectBenefitName() {
        return directBenefitName;
    }

    public void setDirectBenefitName(String directBenefitName) {
        this.directBenefitName = directBenefitName;
    }

    public EDirectBenefits getDirectBenefitType() {
        return directBenefitType;
    }

    public void setDirectBenefitType(EDirectBenefits directBenefitType) {
        this.directBenefitType = directBenefitType;
    }
}
