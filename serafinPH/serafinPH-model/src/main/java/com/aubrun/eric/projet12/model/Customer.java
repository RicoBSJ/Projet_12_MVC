package com.aubrun.eric.projet12.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class Customer {

    private Integer customerId;
    private String nomenclatureUser;
    private String customerFirstName;
    private String customerLastName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateBirth;
    private Long socialSecurityNumber;
    private String mutualName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime entryDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime releaseDate;
    private Integer age;
    private Set<JuridicProtection> juridicProtectionList;
    private Set<Establishment> establishmentList;
    private List<Needs> needsList;
    private List<DirectBenefits> directBenefitsList;
    private List<IndirectBenefits> indirectBenefitsList;

    public Customer() {
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getNomenclatureUser() {
        return nomenclatureUser;
    }

    public void setNomenclatureUser(String nomenclatureUser) {
        this.nomenclatureUser = nomenclatureUser;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public LocalDate getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(LocalDate dateBirth) {
        this.dateBirth = dateBirth;
    }

    public Long getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(Long socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getMutualName() {
        return mutualName;
    }

    public void setMutualName(String mutualName) {
        this.mutualName = mutualName;
    }

    public LocalDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Set<JuridicProtection> getJuridicProtectionList() {
        return juridicProtectionList;
    }

    public void setJuridicProtectionList(Set<JuridicProtection> juridicProtectionList) {
        this.juridicProtectionList = juridicProtectionList;
    }

    public Set<Establishment> getEstablishmentList() {
        return establishmentList;
    }

    public void setEstablishmentList(Set<Establishment> establishmentList) {
        this.establishmentList = establishmentList;
    }

    public List<Needs> getNeedsList() {
        return needsList;
    }

    public void setNeedsList(List<Needs> needsList) {
        this.needsList = needsList;
    }

    public List<DirectBenefits> getDirectBenefitsList() {
        return directBenefitsList;
    }

    public void setDirectBenefitsList(List<DirectBenefits> directBenefitsList) {
        this.directBenefitsList = directBenefitsList;
    }

    public List<IndirectBenefits> getIndirectBenefitsList() {
        return indirectBenefitsList;
    }

    public void setIndirectBenefitsList(List<IndirectBenefits> indirectBenefitsList) {
        this.indirectBenefitsList = indirectBenefitsList;
    }
}
