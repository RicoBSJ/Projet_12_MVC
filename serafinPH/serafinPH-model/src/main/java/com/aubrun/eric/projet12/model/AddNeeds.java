package com.aubrun.eric.projet12.model;

public class AddNeeds {

    private Integer needsId;
    private Integer customerId;

    public Integer getNeedsId() {
        return needsId;
    }

    public void setNeedsId(Integer needsId) {
        this.needsId = needsId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
}
