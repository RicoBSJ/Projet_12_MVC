package com.aubrun.eric.projet12.model;

public class DeleteNeedsForm {

    private Integer customerNeedsId;

    public Integer getCustomerNeedsId() {
        return customerNeedsId;
    }

    public void setCustomerNeedsId(Integer customerNeedsId) {
        this.customerNeedsId = customerNeedsId;
    }
}
