package com.aubrun.eric.projet12.model;

public enum EIndirectBenefits {

    MANAGE_AND_COOPERATION_FUNCTION,
    LOGISTIC_FUNCTION
}
