package com.aubrun.eric.projet12.model;

public class Establishment {

    private Integer establishmentId;
    private EEstablishment establishmentName;

    public Establishment() {
    }

    public Integer getEstablishmentId() {
        return establishmentId;
    }

    public void setEstablishmentId(Integer establishmentId) {
        this.establishmentId = establishmentId;
    }

    public EEstablishment getEstablishmentName() {
        return establishmentName;
    }

    public void setEstablishmentName(EEstablishment establishmentName) {
        this.establishmentName = establishmentName;
    }
}
