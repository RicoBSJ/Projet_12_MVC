package com.aubrun.eric.projet12.model;

public class AddRole {

    private Integer roleId;
    private Integer nomenclatureUserId;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getNomenclatureUserId() {
        return nomenclatureUserId;
    }

    public void setNomenclatureUserId(Integer nomenclatureUserId) {
        this.nomenclatureUserId = nomenclatureUserId;
    }
}
