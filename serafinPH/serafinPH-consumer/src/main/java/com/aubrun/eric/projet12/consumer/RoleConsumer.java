package com.aubrun.eric.projet12.consumer;

import com.aubrun.eric.projet12.model.JwtToken;
import com.aubrun.eric.projet12.model.Role;
import com.aubrun.eric.projet12.model.Roles;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RoleConsumer {

    private final RestTemplate restTemplate;

    public RoleConsumer(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<Roles> findAllRoles(JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/roles/", HttpMethod.GET, entity, Roles.class);
    }

    public ResponseEntity<Role> roleById(int roleId){
        return restTemplate.getForEntity("http://back:8081/projet_12_api/roles/"+ roleId, Role.class);
    }
}
