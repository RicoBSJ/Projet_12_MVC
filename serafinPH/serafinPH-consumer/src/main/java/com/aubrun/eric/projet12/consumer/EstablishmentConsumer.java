package com.aubrun.eric.projet12.consumer;

import com.aubrun.eric.projet12.model.Establishment;
import com.aubrun.eric.projet12.model.Establishments;
import com.aubrun.eric.projet12.model.JwtToken;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class EstablishmentConsumer {

    private final RestTemplate restTemplate;

    public EstablishmentConsumer(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<Establishments> findAllEstablishments(JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/establishments/", HttpMethod.GET, entity, Establishments.class);
    }

    public ResponseEntity<Establishment> establishmentById(int establishmentId) {
        return restTemplate.getForEntity("http://back:8081/projet_12_api/establishments/"+ establishmentId, Establishment.class);
    }
}
