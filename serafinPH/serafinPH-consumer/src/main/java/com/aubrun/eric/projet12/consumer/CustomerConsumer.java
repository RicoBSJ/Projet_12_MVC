package com.aubrun.eric.projet12.consumer;

import com.aubrun.eric.projet12.model.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CustomerConsumer {

    private final RestTemplate restTemplate;

    public CustomerConsumer(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<Customers> findAllCustomers(JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/", HttpMethod.GET, entity, Customers.class);
    }

    public ResponseEntity<Customers> findAllCustomersModerator(JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/moderatorAccess", HttpMethod.GET, entity, Customers.class);
    }

    public ResponseEntity<Customer> customerById(int customerId, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/" + customerId, HttpMethod.GET, entity, Customer.class);
    }

    public ResponseEntity<Void> createCustomer(Customer customer, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Customer> entity = new HttpEntity<>(customer, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/", HttpMethod.POST, entity, Void.class);
    }

    public ResponseEntity<Void> addNomenclatureUser(NomenclatureUser nomenclatureUser, int customerId, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<NomenclatureUser> entity = new HttpEntity<>(nomenclatureUser, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/addNomenclatureUser/" + customerId, HttpMethod.PUT, entity, Void.class);
    }

    public ResponseEntity<Void> addNeeds(int customerId, int needsId, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Integer> entity = new HttpEntity<>(needsId, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/addNeeds/" + customerId, HttpMethod.PUT, entity, Void.class);
    }

    public ResponseEntity<Void> addDirectBenefits(int customerId, int directBenefitsId, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Integer> entity = new HttpEntity<>(directBenefitsId, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/addDirectBenefits/"+ customerId, HttpMethod.PUT, entity, Void.class);
    }

    public ResponseEntity<Void> addIndirectBenefits(int customerId, int indirectBenefitsId, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Integer> entity = new HttpEntity<>(indirectBenefitsId, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/addIndirectBenefits/"+ customerId, HttpMethod.PUT, entity, Void.class);
    }

    public ResponseEntity<Void> updateCustomer(Customer customer, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Customer> entity = new HttpEntity<>(customer, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/addReleaseDate/" + customer.getCustomerId(), HttpMethod.PUT, entity, Void.class);
    }

    public ResponseEntity<Void> updateJuridicProtection(int customerId, int juridicProtectionId, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Integer> entity = new HttpEntity<>(juridicProtectionId, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/addJuridicProtections/" + customerId, HttpMethod.PUT, entity, Void.class);
    }

    public ResponseEntity<Void> updateEstablishment(int customerId, int establishmentId, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Integer> entity = new HttpEntity<>(establishmentId, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/addEstablishmentCustomer/" + customerId, HttpMethod.PUT, entity, Void.class);
    }

    public ResponseEntity<Customers> searchCustomer(SearchCustomer searchCustomer, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<SearchCustomer> entity = new HttpEntity<>(searchCustomer, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/search", HttpMethod.POST, entity, Customers.class);
    }

    public ResponseEntity<Void> deleteJuridicProtection(int customerJuridicProtectionId, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/delJuridicProtection/" + customerJuridicProtectionId, HttpMethod.DELETE, entity, Void.class);
    }

    public ResponseEntity<Void> deleteEstablishment(int customerEstablishmentId, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/delEstablishment/" + customerEstablishmentId, HttpMethod.DELETE, entity, Void.class);
    }

    public ResponseEntity<Void> delNeeds(int customerNeedsId, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/delNeed/" + customerNeedsId, HttpMethod.DELETE, entity, Void.class);
    }

    public ResponseEntity<Void> delDirectBenefits(int customerDirectBenefitsId, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/delDirectBenefits/"+ customerDirectBenefitsId, HttpMethod.DELETE, entity, Void.class);
    }

    public ResponseEntity<Void> delIndirectBenefits(int customerIndirectBenefitsId, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/customers/delIndirectBenefits/"+ customerIndirectBenefitsId, HttpMethod.DELETE, entity, Void.class);
    }
}
