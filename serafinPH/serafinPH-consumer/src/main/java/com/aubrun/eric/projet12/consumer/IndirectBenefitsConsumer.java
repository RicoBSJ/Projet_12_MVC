package com.aubrun.eric.projet12.consumer;

import com.aubrun.eric.projet12.model.IndirectBenefits;
import com.aubrun.eric.projet12.model.IndirectsBenefits;
import com.aubrun.eric.projet12.model.JwtToken;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class IndirectBenefitsConsumer {

    private final RestTemplate restTemplate;

    public IndirectBenefitsConsumer(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<IndirectsBenefits> findAllIndirectBenefits(JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/indirectBenefits/", HttpMethod.GET, entity, IndirectsBenefits.class);
    }

    public ResponseEntity<IndirectBenefits> indirectBenefitsById(int indirectBenefitsId){
        return restTemplate.getForEntity("http://back:8081/projet_12_api/indirectBenefits/"+ indirectBenefitsId, IndirectBenefits.class);
    }
}
