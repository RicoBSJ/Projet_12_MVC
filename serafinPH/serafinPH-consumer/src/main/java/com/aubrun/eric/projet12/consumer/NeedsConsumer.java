package com.aubrun.eric.projet12.consumer;

import com.aubrun.eric.projet12.model.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class NeedsConsumer {

    private final RestTemplate restTemplate;

    public NeedsConsumer(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<Need> findAllNeeds(JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/needs/", HttpMethod.GET, entity, Need.class);
    }

    public ResponseEntity<Needs> needsById(int needsId) {
        return restTemplate.getForEntity("http://back:8081/projet_12_api/needs/"+ needsId , Needs.class);
    }
}
