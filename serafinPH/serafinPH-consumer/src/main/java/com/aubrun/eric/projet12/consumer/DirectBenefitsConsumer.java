package com.aubrun.eric.projet12.consumer;

import com.aubrun.eric.projet12.model.DirectBenefits;
import com.aubrun.eric.projet12.model.DirectsBenefits;
import com.aubrun.eric.projet12.model.JwtToken;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DirectBenefitsConsumer {

    private final RestTemplate restTemplate;

    public DirectBenefitsConsumer(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<DirectsBenefits> findAllDirectBenefits(JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/directBenefits/", HttpMethod.GET, entity, DirectsBenefits.class);
    }

    public ResponseEntity<DirectBenefits> directBenefitsById(int directBenefitsId){
        return restTemplate.getForEntity("http://back:8081/projet_12_api/directBenefits/"+ directBenefitsId, DirectBenefits.class);
    }
}
