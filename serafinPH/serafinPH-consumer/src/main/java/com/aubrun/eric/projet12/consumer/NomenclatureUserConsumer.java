package com.aubrun.eric.projet12.consumer;

import com.aubrun.eric.projet12.model.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class NomenclatureUserConsumer {

    private final RestTemplate restTemplate;

    public NomenclatureUserConsumer(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<NomenclatureUser> addUserAccount(AddNomenclatureUser nomenclatureUser, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<AddNomenclatureUser> entity = new HttpEntity<>(nomenclatureUser, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/api/auth/signup", HttpMethod.POST, entity, NomenclatureUser.class);
    }

    public ResponseEntity<NomenclatureUser> userById(int userId, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/users/"+ userId, HttpMethod.GET, entity, NomenclatureUser.class);
    }

    public ResponseEntity<JwtResponse> login(NomenclatureUser nomenclatureUser) {
        HttpEntity<NomenclatureUser> entity = new HttpEntity<>(nomenclatureUser);
        try {
            restTemplate.exchange("http://back:8081/projet_12_api/api/auth/signin", HttpMethod.POST, entity, Object.class);
        } catch (HttpClientErrorException e) {
            e.getResponseBodyAsString();
        }
        return restTemplate.exchange("http://back:8081/projet_12_api/api/auth/signin", HttpMethod.POST, entity, JwtResponse.class);
    }

    public ResponseEntity<Void> updateNomenclatureUser(NomenclatureUser nomenclatureUser, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<NomenclatureUser> entity = new HttpEntity<>(nomenclatureUser, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/users/addReleaseDateUser/" + nomenclatureUser.getNomenclatureUserId(), HttpMethod.PUT, entity, Void.class);
    }

    public ResponseEntity<Void> addEstablishment(int nomenclatureUserId, int establishmentId, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Integer> entity = new HttpEntity<>(establishmentId, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/users/addEstablishmentUser/"+ nomenclatureUserId, HttpMethod.PUT, entity, Void.class);
    }

    public ResponseEntity<Void> addRole(int nomenclatureUserId, int roleId, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Integer> entity = new HttpEntity<>(roleId, headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/users/addRole/"+ nomenclatureUserId, HttpMethod.PUT, entity, Void.class);
    }

    public ResponseEntity<Void> deleteEstablishment(int nomenclatureUserEstablishmentId, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/users/delEstablishment/" + nomenclatureUserEstablishmentId, HttpMethod.DELETE, entity, Void.class);
    }

    public ResponseEntity<Void> deleteRole(int nomenclatureUserRoleId, JwtToken jwtToken){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/users/delRole/"+ nomenclatureUserRoleId, HttpMethod.DELETE, entity, Void.class);
    }

    public ResponseEntity<NomenclatureUsers> getUsers(JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://back:8081/projet_12_api/users/", HttpMethod.GET, entity, NomenclatureUsers.class);
    }
}
